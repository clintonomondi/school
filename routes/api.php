<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
    Route::post('getOTP', 'AuthController@getOTP');
    Route::post('verifyOtp', 'AuthController@verifyOtp');
    Route::post('resetPassword', 'AuthController@resetPassword');

    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
        Route::get('/classes', 'DarasaController@classes');

        Route::post('/postClass', 'DarasaController@postClass');

        Route::post('/UpdateClass', 'DarasaController@UpdateClass');

        Route::post('/postStream', 'StreamController@postStream');
        Route::get('/streams', 'StreamController@streams');
        Route::post('/updateStream', 'StreamController@updateStream');

        Route::post('/postDorm', 'DormController@postDorm');
        Route::get('/dorms', 'DormController@dorms');
        Route::post('/UpdateDorm', 'DormController@UpdateDorm');

        Route::post('/postSubject', 'SubjectController@postSubject');
        Route::get('/subjects', 'SubjectController@subjects');
        Route::post('/UpdateSubject', 'SubjectController@UpdateSubject');

        Route::post('/register', 'StudentController@register');
        Route::get('/students', 'StudentController@students');
        Route::get('/getStudent/{id}', 'StudentController@getStudent');

    });
});

Route::post('/submitPhoto', 'StudentController@submitPhoto');
Route::post('/searchStudent', 'StudentController@searchStudent');
Route::post('/loadStream', 'DormController@loadStream');
Route::post('/loadDorm', 'DormController@loadDorm');
