<?php

namespace App\Http\Controllers;

use App\Stream;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StreamController extends Controller
{
    public  function postStream(Request $request){
        $validatedData = $request->validate([
            'class_id' => 'required',
        ]);

        $streams = array();
        foreach($request->streams as $data){
            if(empty($data['name']) || empty($data['code'])){

            }else {
                $streams[] = array(
                    "name" => $data['name'],
                    "code" => $data['code'],
                    "class_id" => $request->class_id,
                );

            }
        }
        Stream::insert($streams);
        return ['status'=>true,'message'=>'Stream submitted successfully'];
    }

    public  function streams(){
        $data=DB::select( DB::raw("SELECT *,
(SELECT NAME FROM darasas B WHERE B.id=A.class_id)darasa,
(SELECT fee FROM darasas B WHERE B.id=A.class_id)fee,
(SELECT COUNT(*) FROM students B WHERE B.stream_id=A.id AND  STATUS='Active')students
 FROM `streams` A") );
        return ['data'=>$data];
    }

    public  function  updateStream(Request $request){
        $validatedData = $request->validate([
            'name' => 'required',
            'code' => 'required',
            'class_id' => 'required',
        ]);
        $data=Stream::find($request->id);
        $data->update($request->all());
        return ['status'=>true,'message'=>'Stream updated successfully'];
    }
}
