<?php

namespace App\Http\Controllers;

use App\Dorm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DormController extends Controller
{
    public  function postDorm(Request $request){
        $validatedData = $request->validate([
            'name' => 'required',
            'code' => 'required',
        ]);
        $data=Dorm::create($request->all());
        return ['status'=>true,'message'=>'Class submitted successfully'];
    }

    public  function dorms(){
        $data=DB::select( DB::raw("SELECT*,
(SELECT COUNT(*) FROM students B WHERE B.dorm_id=A.id AND  STATUS='Active')students
 FROM `dorms` A") );
        return ['data'=>$data];
    }

    public  function  UpdateDorm(Request $request){
        $validatedData = $request->validate([
            'name' => 'required',
            'code' => 'required',
        ]);
        $data=Dorm::find($request->id);
        $data->update($request->all());
        return ['status'=>true,'message'=>'Class updated successfully'];
    }

    public  function loadDorm(Request $request){
        $dom=DB::select( DB::raw("SELECT * FROM `dorms` WHERE gender='$request->gender'") );

        return ['data'=>$dom];
    }

    public  function loadStream(Request $request){
        $dom=DB::select( DB::raw("SELECT * FROM `streams` WHERE class_id='$request->class_id'") );

        return ['data'=>$dom];
    }
}
