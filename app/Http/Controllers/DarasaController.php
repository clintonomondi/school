<?php

namespace App\Http\Controllers;

use App\Darasa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DarasaController extends Controller
{
public  function postClass(Request $request){
    $validatedData = $request->validate([
        'name' => 'required',
        'code' => 'required',
        'fee' => 'required',
    ]);
    $data=Darasa::create($request->all());
    return ['status'=>true,'message'=>'Class submitted successfully'];
}

public  function classes(){
    $data=DB::select( DB::raw("SELECT*,
(SELECT COUNT(*) FROM students B WHERE B.class_id=A.id AND  STATUS='Active')students
 FROM `darasas` A") );
    return ['data'=>$data];
}

public  function  UpdateClass(Request $request){
    $validatedData = $request->validate([
        'name' => 'required',
        'code' => 'required',
        'fee' => 'required',
    ]);
    $data=Darasa::find($request->id);
    $data->update($request->all());
    return ['status'=>true,'message'=>'Class updated successfully'];
}
}
