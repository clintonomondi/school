<?php

namespace App\Http\Controllers;

use App\Darasa;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
            'remember_me' => 'boolean',
        ]);
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
            return ['status'=>false,'message' => 'Invalid username or password'];
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        return ['status'=>true,'access_token' => $tokenResult->accessToken, 'token_type' => 'Bearer','expires_at' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString()];
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return ['message' => 'Successfully logged out'];
    }


    public function user(Request $request)
    {
        return response()->json(['status'=>true,'data'=>$request->user()]);
    }


}
