<?php

namespace App\Http\Controllers;

use App\Subject;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    public  function postSubject(Request $request){
        $validatedData = $request->validate([
            'name' => 'required',
            'code' => 'required',
        ]);
        $data=Subject::create($request->all());
        return ['status'=>true,'message'=>'Subject submitted successfully'];
    }

    public  function subjects(){
        $data=Subject::orderBy('id','asc')->get();
        return ['data'=>$data];
    }

    public  function  UpdateSubject(Request $request){
        $validatedData = $request->validate([
            'name' => 'required',
            'code' => 'required',
        ]);
        $data=Subject::find($request->id);
        $data->update($request->all());
        return ['status'=>true,'message'=>'Subject updated successfully'];
    }
}
