<?php

namespace App\Http\Controllers;

use App\Parents;
use App\Photo;
use App\Student;
use App\StudentFee;
use App\StudentSubject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StudentController extends Controller
{
    public  function register(Request $request){
        $regno='';
     if(strlen($request->phone)!=10){
         return ['status'=>false,'message'=>'The parent phone number must be 10 characters'];
     }
        $count=Student::count();
        if($count<1){
            $regno=1 + 87;
        }
        else {
            $regno = $count + 1 + 87;
        }
       $request['regno']=$regno;
        $student=Student::create($request->all());
        $student=Student::where('regno',$regno)->first();
        $student_id=$student->id;
        $request['student_id']=$student_id;
        $request['phone']="254".substr($request->phone, 1);
        $parent=Parents::create($request->all());
        $fee=StudentFee::create($request->all());

        $subjects = array();
        foreach($request->selected as $data){
            $subjects[] = array(
                "student_id"=>$student_id,
                "subject_id"=>$data
            );
        }
        StudentSubject::insert($subjects);
        return ['status'=>true,'message'=>'Student admitted successfully,please wait as we submit profile photo','id'=>$student_id];
    }

    public  function submitPhoto(Request $request){
        $request->validate([
            'pic' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1999',
        ]);

        $fileNameWithExt=$request->file('pic')->getClientOriginalName();
        $filename=pathinfo($fileNameWithExt,PATHINFO_FILENAME);
        $extesion=$request->file('pic')->getClientOriginalExtension();
        //filename to store
        $fileNameToStore=$filename.'_'.time().'.'.$extesion;
        //uploadimage
        $path=$request->file('pic')->storeAs('/public/avatars',$fileNameToStore);

        $request['photo']=$fileNameToStore;
        $request['student_id']=$request->id;
        $pic=Photo::create($request->all());

        return ['status'=>true,'message'=>'Student submitted photo successfully'];
    }

    public  function students(){
        $contdata = DB::select( DB::raw("SELECT id,fname,oname,regno,home,status,gender,
(SELECT NAME FROM darasas B WHERE B.id=A.class_id)class,
(SELECT NAME FROM dorms C WHERE C.id=A.dorm_id)dorm
 FROM `students` A WHERE STATUS='Active'
") );

        return ['status'=>true,'data'=>$contdata];
    }


    public  function searchStudent(Request $request){
        $data =Student::select('id')->where('regno',$request->regno)->get();
        if(empty($data)){
            return ['status'=>false,'message'=>'student not found'];
        }

        return ['status'=>true,'data'=>$data[0],'message'=>'student found'];
    }

    public  function getStudent($id){
        $detail = DB::select( DB::raw("SELECT *,
(SELECT NAME FROM `dorms` B WHERE B.id=A.dorm_id)dorm,
(SELECT NAME FROM `darasas` B WHERE B.id=A.class_id)class,
(SELECT fee FROM `darasas` B WHERE B.id=A.class_id)fee,
(SELECT NAME FROM `streams` B WHERE B.id=A.stream_id)stream,
(SELECT photo FROM `photos` B WHERE B.student_id=A.id)photo
 FROM `students` A WHERE id='$id'") );

        $subjects = DB::select( DB::raw("SELECT * ,
(SELECT NAME FROM `subjects` B WHERE B.id=A.subject_id)name,
(SELECT CODE FROM `subjects` B WHERE B.id=A.subject_id)code
FROM `student_subjects` A WHERE student_id='$id'") );
        $parent=Parents::where('student_id',$id)->get();


        return ['status'=>true,'detail'=>$detail,'parent'=>$parent,'subjects'=>$subjects];
    }
}
