<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable=['fname','oname','gender','regno','home','address','stream_id','kcpe_marks','kcpe_school','kcpe_year','kcpe_index','kcse_index','kcse_year','class_id','dorm_id','status'];
}
