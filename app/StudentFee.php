<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentFee extends Model
{
    protected  $fillable=['student_id','class_id','amount','payment_method','trans_id'];
}
