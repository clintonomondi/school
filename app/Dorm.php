<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dorm extends Model
{
    protected  $fillable=['name','code','gender'];
}
