<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parents extends Model
{
    protected  $fillable=['name','phone','student_id','email','parent_address'];
}
