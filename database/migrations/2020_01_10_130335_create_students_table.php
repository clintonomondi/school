<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fname');
            $table->string('oname');
            $table->string('regno')->unique();
            $table->string('home');
            $table->string('gender')->nullable();
            $table->string('address');
            $table->string('kcpe_marks');
            $table->string('kcpe_school');
            $table->string('kcpe_year');
            $table->string('kcpe_index');
            $table->string('kcse_index')->nullable();
            $table->string('kcse_year')->nullable();
            $table->string('class_id');
            $table->string('stream_id');
            $table->string('dorm_id')->nullable();
            $table->string('status')->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
