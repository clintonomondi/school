import Vue from 'vue'
import VueRouter from 'vue-router'
import VueAxios from 'vue-axios';
import axios from 'axios';
import VueLoading from 'vuejs-loading-plugin'
import Notifications from 'vue-notification'
import VueSpinners from 'vue-spinners'
import VueFormWizard from 'vue-form-wizard'
import 'vue-form-wizard/dist/vue-form-wizard.min.css'


Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(VueLoading)
Vue.use(Notifications)
Vue.use(VueSpinners)
Vue.use(VueFormWizard)

import App from './view/App'
import  Login from './view/login'
import  Home from './view/Home'
import Classes from './classes/darasa.vue'
import Dorms from './dorms/dorms.vue'
import Subject from './subject/subjects.vue'
import Students from './students/students.vue'
import RegisterStudents from './students/register.vue'
import Profile from './students/profile.vue'
import Stream from './streams/streams.vue'
import Test from './view/test.vue'


const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/home',
            name: 'home',
            component: Home
        },
        {
            path: '/',
            name: 'login',
            component: Login,
            meta: { hideNavigation: true }
        },
        {
            path: '/classes',
            name: 'classes',
            component: Classes
        },
        {
            path: '/dorms',
            name: 'dorms',
            component: Dorms
        },
        {
            path: '/subjects',
            name: 'subjects',
            component: Subject
        },
        {
            path: '/students',
            name: 'students',
            component: Students
        },
        {
            path: '/register',
            name: 'registerstudents',
            component: RegisterStudents
        },
        {
            path: '/streams',
            name: 'streams',
            component: Stream
        },
        {
            path: '/profile',
            name: 'profile',
            component: Profile
        },
    ],
});


const app = new Vue({
    el: '#app',
    components: { App },
    router,
    watch: {
        '$route' (to, from) {
            if(this.$route.path==='/'){

            }
            if(this.$route.path==='/home'){
                let uri =`api/auth/user`;
                axios.get(uri, { headers: { Authorization: 'Bearer '+localStorage.getItem('token') } }).then(response => {
                    $("#profile_name" ).text( response.data.data.name);
                    $("#profile_role" ).text( response.data.data.role);
                }).catch(error => {
                    if (error.response.status === 401) {
                        this.$router.push({name: 'login'});
                        this.$notify({group: 'foo', type: 'error', title: 'error!', text: 'Please login!'});
                    }else{
                        this.$notify({group: 'foo', type: 'error', title: 'System error!', text: "System technical error"});
                    }
                });
            }
        }
    }
});

